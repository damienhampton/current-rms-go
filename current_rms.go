package currentrms

func (c *CurrentRms) GetOpportunities() (*OpportunitiesResponse, error) {
	result := OpportunitiesResponse{}
	err := c.sendRequest("/opportunities", &result)
	return &result, err
}

func (c *CurrentRms) GetMembers() (*MembersResponse, error) {
	result := MembersResponse{}
	err := c.sendRequest("/members", &result)
	return &result, err
}
