package currentrms

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type CurrentRms struct {
	AuthToken  string
	Subdomain  string
	BaseUrl    string
	HTTPClient *http.Client
}

func New(authToken string, subdomain string) *CurrentRms {
	return &CurrentRms{
		AuthToken: authToken,
		Subdomain: subdomain,
		BaseUrl:   "https://api.current-rms.com/api/v1",
		HTTPClient: &http.Client{
			Timeout: 5 * time.Minute,
		},
	}
}

func (c *CurrentRms) sendRequest(url string, result interface{}) (err error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", c.BaseUrl, url), nil)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("X-SUBDOMAIN", c.Subdomain)
	req.Header.Set("X-AUTH-TOKEN", c.AuthToken)

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer func() {
		if tempErr := res.Body.Close(); tempErr != nil {
			err = tempErr
		}
	}()

	// Try to unmarshall into errorResponse
	if res.StatusCode != http.StatusOK {
		var errRes CurrentRmsError
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(strings.Join(errRes.Errors, ","))
		}
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	// Unmarshall and populate result
	if err = json.NewDecoder(res.Body).Decode(&result); err != nil {
		return err
	}
	return nil
}
