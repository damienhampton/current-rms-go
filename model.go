package currentrms

import (
	"time"
)

type Meta struct {
	TotalRowCount int `json:"total_row_count"`
	RowCount      int `json:"row_count"`
	Page          int `json:"page"`
	PerPage       int `json:"per_page"`
}

type Member struct {
	ID             int           `json:"id"`
	UUID           string        `json:"uuid"`
	Name           string        `json:"name"`
	Description    string        `json:"description"`
	Active         bool          `json:"active"`
	MembershipType string        `json:"membership_type"`
	TagList        []interface{} `json:"tag_list"`
	CustomFields   struct {
		AccountsContact      string      `json:"accounts_contact"`
		AccountsEmail        string      `json:"accounts_email"`
		RegNumber            string      `json:"reg_number"`
		PoRequired           string      `json:"po_required"`
		VatNo                string      `json:"vat_no"`
		DateAdded            interface{} `json:"date_added"`
		InvoiceEmail         string      `json:"invoice_email"`
		AllRiskInsurance     string      `json:"all_risk_insurance"`
		HowDidYouHearAboutUs string      `json:"how_did_you_hear_about_us"`
		PipedriveID          string      `json:"pipedrive_id"`
		TradeReference1      string      `json:"trade_reference_1"`
		TradeReference2      string      `json:"trade_reference_2"`
	} `json:"custom_fields"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Membership struct {
		ID      int  `json:"id"`
		OnStop  bool `json:"on_stop"`
		OwnedBy int  `json:"owned_by"`
	} `json:"membership"`
	Emails []interface{} `json:"emails"`
	Phones []interface{} `json:"phones"`
	Links  []interface{} `json:"links"`
}

type OpportunityCustomFields struct {
	CollectionNotes            string `json:"collection_notes"`
	CollectionAddress1         string `json:"collection_address_1"`
	CollectionAddress2         string `json:"collection_address_2"`
	CollectionCity             string `json:"collection_city"`
	CollectionPostcode         string `json:"collection_postcode"`
	DeliveryStart              string `json:"delivery_start"`
	DeliveryEnd                string `json:"delivery_end"`
	DeliveryDurationMins       string `json:"delivery_duration_mins"`
	DeliveryConfirmed          string `json:"delivery_confirmed"`
	CollectionStart            string `json:"collection_start"`
	CollectionEnd              string `json:"collection_end"`
	CollectionDurationMins     string `json:"collection_duration_mins"`
	CollectionConfirmed        string `json:"collection_confirmed"`
	Cinematographer            int    `json:"cinematographer"`
	NumberOfVans               string `json:"number_of_vans"`
	AllDay                     string `json:"all_day"`
	ReasonForCancellationOther string `json:"reason_for_cancellation_-_other"`
	RockAndRoll                string `json:"rock_and_roll"`
	DeliveryTime               string `json:"delivery_time"`
	CollectionTime             string `json:"collection_time"`
	NumberOfMen                string `json:"number_of_men"`
}

type Opportunity struct {
	ID                   int                     `json:"id"`
	StoreID              int                     `json:"store_id"`
	MemberID             int                     `json:"member_id"`
	Subject              string                  `json:"subject"`
	Number               string                  `json:"number"`
	StartsAt             time.Time               `json:"starts_at"`
	EndsAt               time.Time               `json:"ends_at"`
	ChargeStartsAt       time.Time               `json:"charge_starts_at"`
	ChargeEndsAt         time.Time               `json:"charge_ends_at"`
	State                int                     `json:"state"`
	StateName            string                  `json:"state_name"`
	Status               int                     `json:"status"`
	StatusName           string                  `json:"status_name"`
	CustomerCollecting   bool                    `json:"customer_collecting"`
	CustomerReturning    bool                    `json:"customer_returning"`
	Reference            string                  `json:"reference"`
	ExternalDescription  string                  `json:"external_description"`
	DeliveryInstructions string                  `json:"delivery_instructions"`
	DeliverStartsAt      time.Time               `json:"deliver_starts_at"`
	DeliverEndsAt        time.Time               `json:"deliver_ends_at"`
	CollectStartsAt      time.Time               `json:"collect_starts_at"`
	CollectEndsAt        time.Time               `json:"collect_ends_at"`
	CreatedAt            time.Time               `json:"created_at"`
	UpdatedAt            time.Time               `json:"updated_at"`
	CustomFields         OpportunityCustomFields `json:"custom_fields"`

	Member         Member `json:"member"`
	BillingAddress struct {
		Name            string `json:"name"`
		Street          string `json:"street"`
		Postcode        string `json:"postcode"`
		City            string `json:"city"`
		County          string `json:"county"`
		CountryName     string `json:"country_name"`
		AddressTypeName string `json:"address_type_name"`
	} `json:"billing_address"`
	Destination struct {
		Address struct {
			ID          int    `json:"id"`
			Name        string `json:"name"`
			Street      string `json:"street"`
			Postcode    string `json:"postcode"`
			City        string `json:"city"`
			County      string `json:"county"`
			CountryID   int    `json:"country_id"`
			CountryName string `json:"country_name"`
		} `json:"address"`
	} `json:"destination"`
}

type OpportunitiesResponse struct {
	Opportunities []Opportunity `json:"opportunities"`
	Meta          Meta          `json:"meta"`
}

type MembersResponse struct {
	Members []Member `json:"members"`
	Meta    Meta     `json:"meta"`
}

type CurrentRmsError struct {
	Errors []string `json:"errors"`
}
