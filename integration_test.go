//go:build integration
// +build integration

package currentrms_test

import (
	"github.com/joho/godotenv"
	currentrms "gitlab.com/damienhampton/current-rms-go"
	"os"
	"testing"
)

func TestCurrentRmsGetOpportunities(t *testing.T) {
	currentRms := currentrms.New(os.Getenv("RMS_AUTH_TOKEN"), os.Getenv("RMS_SUBDOMAIN"))

	response, err := currentRms.GetOpportunities()
	if err != nil {
		t.Errorf("Expected successful response, but got: %s", err)
		return
	}
	if response.Meta.TotalRowCount <= 0 {
		t.Errorf("Meta: Expected at least one opportunity")
	}
	if len(response.Opportunities) <= 0 {
		t.Errorf("Opp list: Expected at least one oportunity")
		return
	}
	if len(response.Opportunities[0].Subject) <= 0 {
		t.Errorf("Expected subject length to be greater than zero")
	}
}

func TestCurrentRmsGetMembers(t *testing.T) {
	currentRms := currentrms.New(os.Getenv("RMS_AUTH_TOKEN"), os.Getenv("RMS_SUBDOMAIN"))

	response, err := currentRms.GetMembers()
	if err != nil {
		t.Errorf("Expected successful response, but got: %s", err)
		return
	}
	if response.Meta.TotalRowCount <= 0 {
		t.Errorf("Meta: Expected at least one member")
	}
	if len(response.Members) <= 0 {
		t.Errorf("Opp list: Expected at least one member")
		return
	}
	if len(response.Members[0].Name) <= 0 {
		t.Errorf("Expected name length to be greater than zero")
	}
}

func setup() {
	godotenv.Load()
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}
